import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'calculator',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../Calculator/calculator.module').then(m => m.CalculatorPageModule)
          }
        ]
      },
      {
        path: 'about',
        children: [
          {
          path: '',
          loadChildren: () => 
            import('../about/about.module').then(m => m.AboutPageModule)
            }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/calculator',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/calculator',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
