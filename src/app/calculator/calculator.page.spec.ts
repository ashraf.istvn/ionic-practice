import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Calculator } from './calculator.page';

describe('CalculatorPage', () => {
  let component: Calculator;
  let fixture: ComponentFixture<Calculator>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Calculator],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Calculator);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
