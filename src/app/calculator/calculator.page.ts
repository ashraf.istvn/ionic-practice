import { Component } from '@angular/core';
import { stringify } from 'querystring';

@Component({
  selector: 'app-calculator',
  templateUrl: 'calculator.page.html',
  styleUrls: ['calculator.page.scss']
})

export class Calculator {

  /**
   * On user click on button, calculate the result and display output on display
   * @param event : user pressed key
   */
  btnClick(event: any){
    let value = event.target.innerHTML;

    if (value === 'C'){
      document.getElementById('result').innerText = '';
      this.disableAllOperation();

      return;
    }

    let result = document.getElementById('result').innerText;
    document.getElementById('result').innerText = result+value;

    if (value === '=') {

      result = document.getElementById('result').innerText;

      var sum = this.calculate(result);
      document.getElementById('result').innerText = ''+sum;

      return;
    }
    
    if (isNaN(parseInt(value))){
      this.disableAllOperation();
    }else{
      this.enableAllOperation();      
    }
  }

  // Calculate and display the output
  private calculate(result:string) {

    var data = Array.from(result);
    var i = 0, j=0;
    var values = [];
    var operations = [];
    var isNegativ = false;
    // Last operation is '=', we will not count this character
    var length = data.length-1;

    // Initialize array first element
    values[i] = '';

    for (let index = 0; index < length; index++) {

      if (isNaN(parseInt(data[index]))){
        
        if (index == 0) {
          isNegativ = true;
          continue;
        }

        operations[j++] = data[index]; 

        // Initialize new array element
        values[++i] = '';
        continue;
      }

      values[i] += data[index];      
    }
    
    i = 0;
    var sum = parseInt(values[i++]);

    // First number is negative so make the sum as negative 
    if (isNegativ) {
      var sum = -sum;
    }        

    // Calculation 
    operations.forEach(element => {
      
      switch(element) {
        case '/':
          sum /= parseInt(values[i++]);
          break;

        case 'x':
          sum *= parseInt(values[i++]);
          break;
        
        case '+':
          sum += parseInt(values[i++]);
          break;
        
        case '-':
          sum -= parseInt(values[i++]);
          break;
      }
    });

    return sum;
  }

  // Disable all operation button
  private disableAllOperation() {
    var operations = []; 
    operations[0] = <HTMLInputElement> document.getElementById('divide');
    operations[1] = <HTMLInputElement> document.getElementById('multiplication');
    operations[2] = <HTMLInputElement> document.getElementById('add');
    operations[3] = <HTMLInputElement> document.getElementById('subtract');
    operations[4] = <HTMLInputElement> document.getElementById('close');
    operations[5] = <HTMLInputElement> document.getElementById('equal');
    
    for (let index = 0; index < operations.length; index++) {
      operations[index].disabled = true;
    }
  }

  // Enable all operation button
  private enableAllOperation() {
    var operations = []; 
    operations[0] = <HTMLInputElement> document.getElementById('divide');
    operations[1] = <HTMLInputElement> document.getElementById('multiplication');
    operations[2] = <HTMLInputElement> document.getElementById('add');
    operations[3] = <HTMLInputElement> document.getElementById('subtract');
    operations[4] = <HTMLInputElement> document.getElementById('close');
    operations[5] = <HTMLInputElement> document.getElementById('equal');
    
    for (let index = 0; index < operations.length; index++) {
      operations[index].disabled = false;
    } 
  }

}
